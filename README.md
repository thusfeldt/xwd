# xwd

Python script that produces LaTeX code to typeset crosswords.

Usage example:

	python xwd.py src/no6.txt 

This will produce LaTeX on standard output, so you want to do something like this:

	python xwd.py src/no6.txt > no6.tex
	pdflatex no6

The syntax for an input file looks like this:

    % no. 2
    % lines with percentages are ignored
    % Then a single blank line, followed by the grid
    
      T
     OAK
     KO

    OAK: All right around a tree.
    KO: Result from {{OAK}} falling on you.

    TAO: Route.

The answer–clue pairs are listed in the order they appear in the grid; across by rows, the down clues by column.

The input file is supposed to be in UTF-8. Output is 7-bit ASCII.

## Multi-part entries

You can split an entry across the grid by surrounding them by double curlies:

    PART OF {{AN ENTRY}}: Clue for entire entry.
    ...
    {{PART OF}} AN ENTRY:

The clue is typeset with the part that does not begin with curlies; the other clues are empty (and will be replaced by something like “See 23 down”.)

