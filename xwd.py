#!/usr/bin/env python

import fileinput, codecs, latex, re, sys, argparse, gettext, locale
from string import Template
from itertools import ifilter
 
parser = argparse.ArgumentParser(
	description = "Render crosswords using LaTeX.")
parser.add_argument("infile", metavar = "FILE",
	help = "a file containing a crossword definition")

group = parser.add_mutually_exclusive_group()
group.add_argument("-n", "--force-new", default = 0,
	action = "store_const", dest = "style", const = 1,
	help = "assume new-style solution definitions")
group.add_argument("-o", "--force-old",
	action = "store_const", dest = "style", const = 2,
	help = "assume old-style solution definitions")
parser.add_argument("-b", "--british",
	action = "store_true",
	help = "fill in empty grid positions with black boxes")
parser.add_argument("-l", "--lang",
	action = "store", default = 'en',
	help = "two-letter code for output language")
parser.add_argument("-s", "--scale",
	action = "store", type = float, default = 0.8,
	help = "scale the crossword by the given factor")

args = parser.parse_args()

# Localization

filename = 'locale/{0}/LC_MESSAGES/messages.mo'.format(args.lang)
try:
	trans = gettext.GNUTranslations(open(filename, 'rb'))
except IOError:
	trans = gettext.NullTranslations()
trans.install()


# dict of strings eventually substituted into template
out = dict(scale = args.scale) 

latex.register()

# Parse the input into three parts.

(G, across, down) = ([],[],[])

state = -1 # become 0, 1, 2 during parsing
for line in codecs.open(args.infile,encoding='utf-8'):
	line = line.rstrip()
	if not line: 
		state += 1
		continue

	# if the source includes a line like this:
	# % no. <digits>
	# then print the <digits> as the title of the crossword
	match = re.findall(r'\% no. (\d+)',line)
	if match: out['number'] = match[0]
	# skip anything else
	if line[0] == '%': continue

	# Put current line into correct part, depending on state
	(G, across, down)[state].append(line)

if args.style == 0:
	# Autodetect which definition style we should use: if all solution
	# lines contain a colon, use new-style solutions
	for i in across + down:
		if not ':' in i:
			args.style = 2
			break
	else:
		args.style = 1

# Build the crossword

class Position():
	def __init__(self): (self.r, self.c) = (0,-1)
	def __str__(self): return '({0},{1})'.format(self.r,self.c)
	def __iter__(self): return self

	def next(self):
		"""Make r and c point to next nonempty cell in across direction"""
		while (True):
			self.c += 1
			if self.c >= len(G[self.r]): (self.r,self.c) = (self.r+1, 0)
			if self.r == len(G): raise StopIteration
 			if G[self.r][self.c] != ' ': break
		return (self.r, self.c)

non_character = re.compile(r"([^\w]+)", re.UNICODE)
double_bracketed_stuff = re.compile(r"({{.+}})")

answer_list = []     # list of words in the order they appear in the source
answer_dict = dict() # maps parts to answers (useful for multi-parts answers)

class Answer():
	# a.across == true a.grid will be placed across
	# a.full == '{{KILLING}} MAO TSE-DONG', i.e., as it appears in the source fie
	# a.part == 'MAO TSE-DONG'
	# a.grid == 'MAOTSEDONG', i.e., as this part appears in the grid
	# a.__str__ == a.grid.encode('latex')
	# separators is dict of tuples (i,c) such that 
	#	   separator c appears after index i in a.grid:
	# a.separators == [(2,' '), (5,'-')]
	def __init__(self, s, across):
		self.across = across
		self.full = s
		self.part = double_bracketed_stuff.sub("",self.full).strip()
		self.grid = non_character.sub("",self.part)
		l = [x.start() for x in re.finditer(non_character, self.part)]    # l = [3,7]
		l = [x - y for (x,y) in zip(l, range(1,len(self.part)+1))]        # l = [3-1,7-2] = [2,5]
		self.separators = zip(l, [x.group() for x in re.finditer(non_character, self.part)]) # zip with [' ','-']
		# register the word in list of answers and dictionary of parts
		answer_list.append(self)
		answer_dict[self.part] = self

	def __str__(self): return self.grid.encode('latex')

	def check(self,pos):
		"""Return True iff word matches at the current position."""
		return G[pos.r].find(self.grid,pos.c) == pos.c 
		
	def length_indicator(self):
		s = re.sub('[{}]','',self.full,0)
		result = []
		i = False
		for seg in filter(lambda x: len(x) != 0, non_character.split(s)):
			result.append(
				str(len(seg)) if not i
				else ("," if seg == " " else seg.strip()))
			i = not i
		return "({0})".format("".join(result))	

	def _complex_clue_counter(self):
		""" For 'HOT FOR {{MAO}} {{TSE-DONG}}' returns ['5a', '5'],
			where HOT FOR is at 3d, TSE-DONG at 5d, and MAO at 5a.
		"""
		L = []
		for part in re.findall("{{(.*?)}}", self.full):
			a = answer_dict[part]
			if a.across == self.across: L.append(str(a.counter))
			else: L.append(a.pos_str_short())
		return ' '.join(L)


	def formatted_clue(self):
		""" Return nicely formatted clue for final output
			(This only makes sense after the entire crossword is computed,
			so self.counter is known and double-braces
			in clues[self] have been resolved)."""
		result = "{{\\bf {0}}}~".format(self.counter)
		if self.full.find('{') >= 0: # multi-part answer
			if self.full.find('{') == 0: # not the leading entry
				head_part = re.search("{{(.*?)}}",self.full).group(1)
				return result + _('See') + ' ' + answer_dict[head_part].pos_str() + '.\n'
			else:
				result += "{{\\bf {0}}}~".format(self._complex_clue_counter())
		return result + "{0}~\mbox{{{1}}}\n".format(clues[a], a.length_indicator())

	def pos_str(self):
		return str(self.counter) + ' ' + (_('down'),_('across'))[self.across]

	def pos_str_short(self):
		return str(self.counter) + (_('d'),_('a'))[self.across]

class Clue():
	def __init__(self, s): self.s = s

	def	__str__(self): return self.s.encode('latex')

def place(a, pos, turned):
	gword= a.grid
	assert(a.check(pos))
	if not turned:
		beg[('acrs', pos.r, pos.c)] = a
		end[('acrs', pos.r, pos.c + len(gword))] = True
		for (i,c) in a.separators:
			sep[('acrs', pos.r, pos.c + i)] = c
	else:
		beg[('down', pos.c, pos.r)] = a
		end[('down',pos.c + len(gword), pos.r)] = True
		for (i,c) in a.separators:
			sep[('down', pos.c + i, pos.r)] = c
	pos.c += len(gword)
#	print "Placed {0} at {1}".format(a, str(pos))

# Place the across and down clues 


beg = {} # beg[('down',r,c)] == 'OAK' if OAK (down) starts here
end = {} # end[('acrs',r,c)] == True if an across-word ends here
sep = {} # end[('acrs',r,c)] == True if a separators (space, hyphen) should be drawn to the right
clues = {}    # clues['OAK'] = Clue('All right around the tree.')

# populate clues dictionary, create Word objects for every word

def parse_lines(lines, across = True):
	for line in lines:
		segments = line.split(":" if args.style == 1 else " ", 1)
		a = Answer(segments[0].strip(),across)
		c = Clue(segments[1].strip()) 
		clues[a] = c

parse_lines(across)
parse_lines(down, across = False)

def place_words_horizontally(pos, across = True):
	""" Place words in list in grid in the across direction """
	for a in ifilter(lambda a: a.across == across, answer_list):
		try: 
			while (not a.check(pos)): pos.next()
			place(a, pos, not across)
		except StopIteration:
			print("exhausted grid without placing {0}".format(a))

# Place words across them in the grid one by one
pos = Position()
pos.next()
place_words_horizontally(pos)
 	
# Now place words down. 
# But that's the same as placing across in a transposed grid(!)

G = [line.ljust(len(max(G,key=len))) for line in G] # pad all lines in G to uniform length
tmp = G
G = map(''.join, zip(*G)) # mind == blown
pos = Position()
pos.next()
place_words_horizontally(pos, across = False)

G = tmp
(acrosscounter, downcounter) = ({},{})

# Print the puzzle
# Unremarkable TikZ 

L = []
cluecounter = 1
if args.british:
	L.append('\\fill (-.5,+.5) rectangle ({0},{1});'.format(len(G)-.5, -len(max(G,key=len))+.5))
for (r,c) in Position():
	L.append('\\node[minimum size = 1cm, draw, fill=white] at ({0},{1}) {{}};'.format(c, -r))
	clue_starts_here = False
	if ('acrs',r,c) in beg:
		a = beg[('acrs',r,c)]
		assert (a.across)
		a.counter = cluecounter
		clue_starts_here = True
	if ('down',r,c) in beg:
		a = beg[('down',r,c)] 
		assert (not a.across)
		a.counter = cluecounter
		clue_starts_here = True
	if clue_starts_here: 
		L.append('\\node[anchor = north west, font = \\sf\\footnotesize] at ({0},{1}) {{{2}}};'.format(c-.5,-r+.5,cluecounter))
		cluecounter += 1

	if ('acrs',r,c) in beg and c > 0 and G[r][c-1] != ' ':
		L.append('\\draw [ultra thick] ({0},{1}) -- ({0},{2});'.format(c-.5,-r+.5,-r-.5))
	if ('down',r,c) in beg and r > 0 and G[r-1][c] != ' ': 
		L.append('\\draw [ultra thick] ({0},{1}) -- ({2},{1});'.format(c-.5,-r+.5,c+.5))
	if ('acrs',r,c) in end and c < len(G[r])-1 and G[r][c+1] != ' ':
		L.append('\\draw [ultra thick] ({0},{1}) -- ({0},{2});'.format(c+.5,-r+.5,-r-.5))
	if ('down',r,c) in end and r < len(G)-1 and G[r+1][c] != ' ': # this could go wrong!
		L.append('\\draw [ultra thick] ({0},{1}) -- ({2},{1});'.format(c-.5,-r-.5,c+.5))

# draw the separators last:

for (r,c) in Position():
	if ('acrs',r,c) in sep:
		if sep[('acrs',r,c)] == ' ':
			L.append('\\draw [ultra thick] ({0},{1}) -- ({0},{2});'.format(c+.5,-r+.5,-r-.5))
		if sep[('acrs',r,c)] == '-':
			L.append('\\draw [ultra thick] ({0},{1}) -- ({2},{1});'.format(c+.4,-r,c+.6))

	if ('down',r,c) in sep:
		if sep[('down',r,c)] == ' ':
			L.append('\\draw [ultra thick] ({0},{1}) -- ({2},{1});'.format(c-.5,-r-.5,c+.5))
		if sep[('down',r,c)] == '-':
			L.append('\\draw [ultra thick] ({0},{1}) -- ({0},{2});'.format(c,-r-.4,-r-.6))

out['crossword'] = '\n'.join(L)

# We turn to the clues.
# Replace placeholders like '{{OAK}}' with their coordinate like '20 vandret'

for c in clues.itervalues():
	for part in re.findall(r'\{\{(.*?)\}\}',c.s):  # look for '{{OAK}}', iterate over 'OAK'
		c.s = c.s.replace('{{' + part + '}}', answer_dict[part].pos_str())

# Output the clues

answer_list   = sorted(answer_list, key = lambda a: a.counter)
out['across_header'] = _('Across')
out['across'] = '\n'.join([a.formatted_clue() for a in answer_list if a.across])
out['down_header'] = _('Down')
out['down']   = '\n'.join([a.formatted_clue() for a in answer_list if not a. across])

class LaTeXTemplate(Template): delimiter = '@'
t = LaTeXTemplate(open('templates/page.tex').read())
print t.substitute(out)
